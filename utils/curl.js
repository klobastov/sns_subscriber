const Crawler = require("crawler");

module.exports = function(success){
    return new Crawler({
        maxConnections : 10,
        callback:success
    });
};