module.exports = function(req, res, next){
    if (req.is('text/*')) {
        req.text = '';
        req.setEncoding('utf8');
        req.on('data', function(chunk){ 
            req.text += chunk.replace(/([\n\\ ])/g, '');
        });
        req.on('end', function(){
            req.text = JSON.parse(req.text
                .replace(/"{/g, '{')
                .replace(/}"/g, '}'));
            next();
        });
    } else {
	    req.text = '';
        next();
    }
};