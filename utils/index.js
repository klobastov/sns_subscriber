var autoload = {
    _f:[],
    init:function() {
        this._f = App.fs.readdirSync(__dirname);
        if ("object"===typeof this._f) for (var a in this._f) 
            "index.js"!==this._f[a]&&(module.exports[this._f[a].split(".")[0]]
                = require([__dirname, this._f[a]].join("/")));
    }
};
autoload.init();

    