module.exports = function(){
    let message = [];
    for(let id in arguments){
        switch(App.typeof(arguments[id])){
            case 'array':
            case 'object':
                arguments[id] = JSON.stringify(arguments[id]);
            break;
        }
        message.push(arguments[id]);
    }
    console.log(App.moment().format('MM-DD-YYYY HH:mm:ss'), message.join(' '));
};