const AWS = require('aws-sdk');
const exec = require('child_process').exec;

module.exports = function (req, res) {
    App.util.log('Request body:', req.text);

    if (req.text.length === 0) {
        App.util.log('Empty Message. Aborting.');
        res.sendStatus(200);

        return false;
    }

    AWS.config.update({
        accessKeyId: App.config.aws.access_key_id,
        secretAccessKey: App.config.aws.secret_access_key,
        region: req.text.Message.region
    });

    const ec2 = new AWS.EC2({apiVersion: '2016-11-15'});
    ec2.describeInstances({InstanceIds: [req.text.Message.detail['instance-id']]}, function (err, data) {
        if (err) {
            App.util.log(err);
            res.sendStatus(200);
        }
        if (typeof data.Reservations[0].Instances[0].PublicIpAddress !== 'undefined') {
            if ('running' === req.text.Message.detail.state) {
                exec("/opt/node/iptables/farm_ip_changer.sh -a=" + data.Reservations[0].Instances[0].PublicIpAddress, function (err, stdout, stderr) {
                    if (err) {
                        App.util.log(stderr);
                    }
                });
            }

            if ('terminated' === req.text.Message.detail.state) {
                exec("/opt/node/iptables/farm_ip_changer.sh -r=" + data.Reservations[0].Instances[0].PublicIpAddress, function (err, stdout, stderr) {
                    if (err) {
                        App.util.log(stderr);
                    }
                });
            }
        }
        res.sendStatus(200);
    });
};