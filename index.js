var express = require('express'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override');

App = express();
App.server = require('http').Server(App);
App.fs = require('fs');
App.path = require('path');
App.typeof = require('typeof');
App.exec = require('child_process').exec;
App.async = require('async');
App.querystring = require('querystring');
App.moment = require('moment');
App.os = require('os');
App.config = require('config-node')();
App.io = require('socket.io')(App.server);
App.util = require(__dirname + '/utils');
App.action = require(__dirname + '/actions');

App.use(bodyParser.urlencoded({
    extended: true
}));

App.use(bodyParser.json());
App.use(methodOverride());
App.use(App.util.request);

// App.get('/subscribe', App.action.subscribe);
App.post('/subscribe', App.action.subscribe);

App.server.listen(App.config.server.port, function () {
    App.util.log('Subscriber start on port ' + App.config.server.port);
});
